require('./commands')

Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})

declare namespace Cypress {
  interface Chainable {
    interceptAndWait(url: string, alias?: string, timeOut?: number): Chainable<Element>;
  }
}

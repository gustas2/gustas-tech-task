/// <reference types="cypress" />

Cypress.Commands.add('interceptAndWait', (url, alias = 'tempAlias', timeOut = 60000) => {
    return cy.intercept(url).as(alias).wait(`@${alias}`, {timeout: timeOut})
})


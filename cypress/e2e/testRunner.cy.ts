import { checkPricesAndDiscount } from "./ui/Atoms/familyPage"
import {checkHrefElementExistAndVisible, landingPage } from "./ui/Atoms/landingPage"
import { selectAndCheckPricingCart} from "./ui/Atoms/MailPage/pricingPage"
import { navigateFromLandingToEmailPricing, navigateFromLandingToFamilyPricing } from "./ui/Atoms/navigations"

context('Proton Mail pricing model tests', () => {
    beforeEach(() => {
      cy.clearCookies()
      cy.visit('/')
      /* This is the last API call made when the landing page is loaded 
        so I wait for it to get called before I start my test so I know the
        page is fully loaded */ 
      cy.interceptAndWait('/page-data/mail/*', 'alias1');
    });
    
    /*
    TEST CASE 1
    Action: User lands on the Proton homepage.
    Expected Outcome: Landing page has all the expected access points to the email/email-pricing section.
    Steps:
    Navigate to https://proton.me/.
    Observe the homepage layout to locate elements redirecting to the email/email-pricing section.
    */
    it('User can easily locate and access the email section', () => {
        /* This part checks how many elements in the landing page 
        that are visible to the user contain href='/mail' or '/mail/pricing' parameter' */
        cy.get(landingPage.HREF_MAIL_PRICING_EL)
        .filter(':visible')
        .should('have.length', 4);
        cy.get(landingPage.HREF_MAIL_PAGE_EL)
        .should('have.length', 5)
        .filter(':visible')
        .should('have.length', 3);
        
        /* This part checks if the nested element withing the top bar navigation
        of href='/mail' can be accesed and is visible to the user */
        cy.get(landingPage.HEADER.NAVBAR).within(() => {
            cy.get('button').eq(0)
            .as('productsBtn')
            .should('be.visible')
            .click();
            cy.get(landingPage.HEADER.POPOVER+':visible')
            .within(() => {
                cy.get(landingPage.HREF_MAIL_PAGE_EL)
                .should('be.visible')
            });
            cy.get('@productsBtn').click();
        });

        /* This part checks that each of the elements that contain
        href='/mail' or '/mail/pricing' parameter on the landing page are visible to the user.
        It is a BAD practice to group so many assertions of different elements in one test
        but I'm doing it to not have to write a separate test for each element */
        const parameters = [
            { frame: landingPage.HEADER.FRAME, element: landingPage.HREF_MAIL_PRICING_EL, link: landingPage.HREF_MAIL_PRICING_LINK },
            { element: landingPage.BODY.HERO.NAVBAR.MAIL, link: landingPage.HREF_MAIL_LINK },
            { element: landingPage.BODY.HERO.BUTTONS.MAIL, link: landingPage.HREF_MAIL_LINK },
            { frame: landingPage.BODY.SECTION1.FRAME, index: 0, element: landingPage.BODY.SECTION1.BUTTON, link: landingPage.HREF_MAIL_PRICING_LINK },
            { frame: landingPage.BODY.SECTION2.FRAME, index: 1, element: landingPage.BODY.SECTION2.BUTTON, link: landingPage.HREF_MAIL_PRICING_LINK },
            { frame: landingPage.BODY.SECTION2.FRAME, index: 2, element: landingPage.BODY.SECTION3.BUTTON, link: landingPage.HREF_MAIL_PRICING_LINK },
            { frame: landingPage.FOOTER.FRAME, element: landingPage.HREF_MAIL_PAGE_EL, link: landingPage.HREF_MAIL_LINK }
          ];
          
          parameters.forEach(param => {
            if (param.frame) {
              cy.get(param.frame).eq(param.index || 0).within(() => {
                checkHrefElementExistAndVisible(param.element, param.link);
              });
            } else {
              checkHrefElementExistAndVisible(param.element, param.link);
            }
          });
    })

    /*
    TEST CASE 2
    Action: Exploring Pricing Model with Individual Plans
    Expected Outcome:
    The user should be able to view various Individual Plan options.
    Details such as features included, pricing should be visible.
    Currency options and billing periods should be displayed accurately.
    Steps:
    Navigate to the Proton Mail website (https://proton.me/mail/pricing).
    Locate and click on the "Individuals" section.
    Verify the presence of "Individual Plans".
    NOTE: I'm only veryfing price Plan name within different currencies and periods for DEMO purposes
    */
    it('User can navigate to email/individuals plans section and see the correct information', () => {
        navigateFromLandingToEmailPricing();

        const parameters = [
            { type: 'individuals', period: 1, currency: 'chf' },
            { type: 'individuals', period: 12, currency: 'eur' },
            { type: 'individuals', period: 24, currency: 'usd' },
          ];
          
          parameters.forEach(param => {
            selectAndCheckPricingCart(param.type, param.period, param.currency);
          });
    })

     /*
    TEST CASE 3
    Action: Exploring Pricing Model with Business Plans
    Expected Outcome:
    The user should be able to view various Business Plans options.
    Details such as features included, pricing should be visible.
    Currency options and billing periods should be displayed accurately.
    Steps:
    Navigate to the Proton Mail website (https://proton.me/).
    Locate and click on the "Business" section.
    Verify the presence of "Business Plans".
    NOTE: I'm only veryfing price and Plan name within different currencies and periods for DEMO purposes
    */
    it('User can navigate to email/business plans section and see the correct information', () => {
        navigateFromLandingToEmailPricing();

        const parameters = [
            { type: 'businesses', period: 1, currency: 'chf' },
            { type: 'businesses', period: 12, currency: 'eur' },
            { type: 'businesses', period: 24, currency: 'usd' }
          ];
          
          parameters.forEach(param => {
            selectAndCheckPricingCart(param.type, param.period, param.currency);
          });
    })

    /*
    TEST CASE 4
    Action: Exploring Pricing Model with Family Plans
    Expected Outcome:
    The user should be able to view Family pricing options.
    Details such as features included, pricing should be visible.
    Currency options and billing periods should be displayed accurately.
    Steps:
    Navigate to the Proton Mail website (https://proton.me/).
    Locate and click on the "Families" section.
    Verify the presence of "Family" section.
    NOTE: I'm only veryfing price and discount applied within different currencies for DEMO purposes
    */
    it('User can navigate to email/family section and see the correct information', () => {
        navigateFromLandingToFamilyPricing();

        const parameters = [
            { currency: 'chf' },
            { currency: 'eur' },
            { currency: 'usd' }
          ];

        parameters.forEach(param => {  
            checkPricesAndDiscount(param.currency);
        });
    });
  })
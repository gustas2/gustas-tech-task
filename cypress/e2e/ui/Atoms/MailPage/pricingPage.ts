const pricingPage = {
    PLAN_SWITCHER_TOP: {
        FRAME: '[data-testid="pricing-page-switcher-top"]',
        INDIVIDUALS: '[href="/mail/pricing"]',
        BUSINESS: '[href="/business/plans"]',
        FAMILY: '[href="/family"]'
    },
    PERIOD_SWITCHER_TOP: {
        FRAME: '[data-test="cycle-selector"]',
        ONE_MONTH: '[value="1"]',
        TWELVE_MONTHS: '[value="12"]',
        TWENTY_FOUR_MONTHS: '[value="24"]',
    },
    PRICING_CARDS: {
        FRAME_INDIVIDUALS: '[data-testid="family-cards"]',
        FRAME_BUSINESS: '[data-test="b2b-cards"]',
        CARD: 'div.grid',
    },
    CURRENCY_SELECTOR: {
        SELECTOR: '[data-testid="select-id"]'
    },
    LANGUAGE_SELECTOR: {
        BUTTON: '[data-testid="menu-button-trigger"]',
        LIST: '[data-test-id="menubutton-list"]'
    },
};

type pricingPage = (typeof pricingPage)[keyof typeof pricingPage];

const selectPlan = (plan: string): void => {
    cy.get(pricingPage.PLAN_SWITCHER_TOP.FRAME+':visible').first().within(() => {
        if (plan === 'individuals') cy.get(pricingPage.PLAN_SWITCHER_TOP.INDIVIDUALS).click();
        if (plan === 'businesses') {
            cy.get(pricingPage.PLAN_SWITCHER_TOP.BUSINESS).click();
            cy.interceptAndWait('/business/plans*', 'pageData');
        }
    })
};

const selectPeriod = (period: number): void => {
    cy.get(pricingPage.PERIOD_SWITCHER_TOP.FRAME+':visible').within(() => {
        if (period === 1) cy.get(pricingPage.PERIOD_SWITCHER_TOP.ONE_MONTH).click({timeout: 5000, force: true});
        if (period === 12) cy.get(pricingPage.PERIOD_SWITCHER_TOP.TWELVE_MONTHS).click({timeout: 5000, force: true});
        if (period === 24) cy.get(pricingPage.PERIOD_SWITCHER_TOP.TWENTY_FOUR_MONTHS).click({timeout: 5000, force: true});
    })
};

const selectCurrency = function(currency: string): void {
    cy.get(pricingPage.CURRENCY_SELECTOR.SELECTOR).select(currency.toUpperCase());
};

const selectLanguage = function(language: string): void {
    cy.get(pricingPage.LANGUAGE_SELECTOR.BUTTON).click();
    cy.get(pricingPage.LANGUAGE_SELECTOR.LIST).contains(language).click();
};

const checkName = (element: any, name: string) => {
    cy.get(element).then(element => {
        expect(element).to.contain(name);
    })
};

const checkPrice = (element: any, json: any, period: number, currency: string) => {
    const periodMap: { [key: number]: string } = {
        1: 'month',
        12: 'twelveMonths',
        24: 'twentyFourMonths',
    };
    if (['eur', 'usd', 'chf'].includes(currency) && periodMap[period]) {
        cy.get(element).then(element => {
            expect(element).to.contain(json[periodMap[period]][currency]);
        });
    } else {
        console.log('Invalid period or currency');
    }
}

const selectAndCheckPricingCart = (pricing: string, period: number, currency: string = '', language: string = 'English'): void => {
    selectPlan(pricing);
    selectCurrency(currency);
    selectPeriod(period);
    selectLanguage(language);   

    cy.fixture(`${pricing}.json`).then((json)=> {
        cy.get(pricing === 'individuals' ? pricingPage.PRICING_CARDS.FRAME_INDIVIDUALS : pricingPage.PRICING_CARDS.FRAME_BUSINESS)
        .children('div')
        .each((element, i) => {
            checkName(element, json[i].name);
            checkPrice(element, json[i].price, period, currency)
            });
        });
    };

export {pricingPage, selectAndCheckPricingCart};
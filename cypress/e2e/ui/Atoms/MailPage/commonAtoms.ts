const mailPageCommon = {
    NAVBAR: {
        FRAME: '[data-test="breadcrumbs-id"]',
        OVERVIEW: '[href="/mail"]',
        SECURITY: '[href="/mail/security"]',
        PRICING: '[href="/mail/pricing"]',
        BRIDGE: '[href="/mail/bridge"]',
        DOWNLOAD: 'href="/mail/download"]',
        SUPPORT: '[href="/support/mail"]',
    },
} as const;

type mailPageCommon = (typeof mailPageCommon)[keyof typeof mailPageCommon];

export {mailPageCommon};
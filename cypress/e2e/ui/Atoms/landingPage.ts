const landingPage = { 
    HEADER:  {
        FRAME: 'header',
        NAVBAR: '[data-testid="header-id"] nav',
        POPOVER: '[data-testid="popover-panel"]',
    },
    BODY: {
        HERO:{
            NAVBAR: {
                MAIL: '[title="Encrypt your emails with Proton Mail"]',
                VPN: '[title="Browse privately with Proton VPN"]',
                CLOUD_STORAGE: '[title="Secure your files with Proton Drive"]',
                PASSOWRD_MANAGER: '[title="Protect your passwords with Proton Pass"]',
                CALENDAR: '[title="Schedule privately with Proton Calendar"]',
            },
            BUTTONS:{
                MAIL: '.bg-btnMail',
                VPN: '.bg-btnVpn',
                CLOUD_STORAGE: '.bg-btnDrive',
                PASSOWRD_MANAGER: '.bg-btnPass',
                CALENDAR: '.bg-btnCalendar',
            },
        },
        SECTION1:{
            FRAME: '[data-testid="content-with-img"]',
            BUTTON: '[data-testid="content-with-image-section-cta-2"]',
        },
        SECTION2:{
            FRAME: '[data-test="title-component"]',
            BUTTON: '[data-testid="title-section-cta"]',
        },
        SECTION3:{
            FRAME:'[data-test="title-component"]',
            BUTTON:'[data-testid="title-section-cta"]',
        },
    },
    FOOTER: {
        FRAME: 'footer',
        MAIL_BUTTON: '[data-testid="footer-mail"]',
    },
    HREF_MAIL_PRICING_EL: '[href="/mail/pricing"]',
    HREF_MAIL_PAGE_EL: '[href="/mail"]',
    HREF_MAIL_LINK: '/mail',
    HREF_MAIL_PRICING_LINK: '/mail/pricing',
} as const;

type landingPage = (typeof landingPage)[keyof typeof landingPage];

const checkHrefElementExistAndVisible = (element: string, href: string) => {
    return cy.get(`${element}:visible`, {timeout: 5000})
    .scrollIntoView({offset: { top: -50, left: 0 }, easing: 'linear' })
    .should('be.visible')
    .should('have.attr', 'href', `${href}`)
};

export {landingPage, checkHrefElementExistAndVisible}
const familyPage = {
    CURRENCY_SELECTOR: '[data-testid="select-id"]'
} as const;

type familyPage = (typeof familyPage)[keyof typeof familyPage];

const selectPricingCards = () => {
    return cy.get('section').find('div').first().children('div').last().get('.p-3', {timeout: 5000});
};

const selectCurrency = (currency: string) => {
    cy.get(familyPage.CURRENCY_SELECTOR).select(currency.toUpperCase());
};

const checkPrice = (element:any, json: any, index : number, currency: string) => {
    const periodMap: { [key: number]: string } = {
        0: 'month',
        1: 'twelveMonths',
        2: 'twentyFourMonths',
    };
    if (['eur', 'usd', 'chf'].includes(currency) && periodMap[index]) {
        cy.get(element).then(element => {
            expect(element).to.contain(json[periodMap[index]][currency]);
        });
    } else {
        console.log('Invalid period or currency');
    };
};

const checkDiscount = (element: any, json: any, i: number) => {
    const periodMap: { [key: number]: string } = {
        2: 'month',
        1: 'twelveMonths',
        0: 'twentyFourMonths',
    };

    cy.get(element).then(element => {
        expect(element).to.contain(json.discount[periodMap[i]]);
    });
};

const checkPricesAndDiscount = (currency: string) => {
    selectCurrency(currency);

    cy.fixture('family.json').then((json) => { 
        selectPricingCards().each((element, i)=> {
            checkPrice(element, json, i, currency)
            checkDiscount(element, json, i)
        });
    });
};


export {familyPage, checkPricesAndDiscount};
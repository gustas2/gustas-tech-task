import { mailPageCommon } from "./MailPage/commonAtoms";
import { pricingPage } from "./MailPage/pricingPage";
import { landingPage } from "./landingPage";

const navigateFromLandingToEmailPricing = () => {
    cy.get(landingPage.BODY.HERO.NAVBAR.MAIL).click();
    cy.url().should('include', '/mail');
    cy.interceptAndWait('page-data/support/mail/*', 'alias2');
    cy.get(mailPageCommon.NAVBAR.FRAME).within(()=>{
        cy.get(mailPageCommon.NAVBAR.PRICING).click();
    })
    cy.url().should('include', '/mail/pricing');
    cy.interceptAndWait('page-data/business/plans/*', 'alias3');
}

const navigateFromLandingToFamilyPricing = () => {
    cy.get(landingPage.BODY.HERO.NAVBAR.MAIL).click();
    cy.url().should('include', '/mail');
    cy.interceptAndWait('page-data/support/mail/*', 'alias2');
    cy.get(mailPageCommon.NAVBAR.FRAME).within(()=>{
        cy.get(mailPageCommon.NAVBAR.PRICING).click();
    })
    cy.url().should('include', '/mail/pricing');
    cy.interceptAndWait('page-data/business/plans/*', 'alias3');
    cy.get(pricingPage.PLAN_SWITCHER_TOP.FRAME+':visible').first().within(() => {
        cy.get(pricingPage.PLAN_SWITCHER_TOP.BUSINESS).click();
        cy.get(pricingPage.PLAN_SWITCHER_TOP.FAMILY).click();
    });
    cy.interceptAndWait('/family*', 'alias4');
    cy.url().should('include', '/family');
}

export {navigateFromLandingToEmailPricing, navigateFromLandingToFamilyPricing}

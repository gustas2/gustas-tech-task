# gustas-paulionis-tech-task-project

## Run the project locally

-   Ensure you have Git installed:
    
    ```
    git --version
    ```
    
    If not, install it in mac with:
    
    ```
    brew install git
    ```

-   Ensure you have sync the git credentials with your GitLab account and you have permissions to the repo:

    ```
    Crate ssh keys: https://docs.gitlab.com/ee/user/ssh.html
    
-   clone this repo to a local directory

    ```
    git clone https://gitlab.com/gustas2/gustas-tech-task.git
    ```

-   cd into the cloned repo

    ```
    cd gustas-tech-task
    ```

-   install the node_modules

    ```
    npm run prepareProject or npm install
    ```

-   open cypress in debug mode
    ```
    npm run cy:openDebug
    ```

-   run cypress tests in headed mode
    ```
    npm run cy:run:chrome:headed
    ```     

 -  run cypress tests in headless mode
    ```
    npm run cy:run:chrome:headless
    ```     

## Components:

1. [Cypress](https://docs.cypress.io/guides/overview/why-cypress): The library that interact with the browser
2. TSLint: The lint library for TypeScript

## Folder structure

```
/cypress
  /fixtures: any data that we will require
  /e2e: cypress code
  /support: cypress custom commands
```

## Understand the architecture

Inside `/e2e`, the folders and files will match the names of the test object;

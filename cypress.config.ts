import { defineConfig } from 'cypress'

export default defineConfig({
  supportFolder: 'cypress/support',
  viewportHeight: 1080,
  viewportWidth: 1920,
  video: false,
  screenshotOnRunFailure: false,
  defaultCommandTimeout: 60000,
  chromeWebSecurity: false,
  retries: 1,
  e2e: {
    baseUrl: 'https://proton.me/',
    specPattern: 'cypress/e2e/**/*.cy.{js,jsx,ts,tsx}',
  },
  experimentalSourceRewriting: true
})

